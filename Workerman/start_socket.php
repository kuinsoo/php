<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
use Workerman\Lib\Timer;


// Create a Websocket server
$ws_worker = new Worker("websocket://0.0.0.0:8888");
$task = new Worker();

$timedata = $task->onWorkerStart;
// 4 processes
$ws_worker->count = 4;
// 4 processes
$web->count = 4;

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
   $JSONConnected = 
   [
      "type"   => "connected",
      "msg"    => "접속성공",
   ];
   $JSONEncode = json_encode($JSONConnected);

   $connection->send($JSONEncode);
 };

// Emitted when data received
$ws_worker->onMessage = function($connection, $data)
{
   // global $task;
   // $decodeData = json_decode($data);
   //  switch ($decodeData->inputText) {
   //     case '시작':
   //       $connection->send("tset");
   //       break;
       
   //     default:
   //        # code...
   //        break;
   //  }
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
    echo "Connection closed\n";
};

// Run worker
Worker::runAll();