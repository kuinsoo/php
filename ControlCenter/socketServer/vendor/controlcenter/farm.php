<?php
namespace ControlCenter;

class Farm
{
    /* 생성자 */
    function __construct(){}

    /* 계약 농가 리스트 */
    public function contractFarmList($connection, $data) 
    {
        // $qFarmList = "SELECT * FROM farm WHERE 1 AND lat <> 0 AND lon <> 0";
        // $sqlFarmList = mysql_query($qFarmList);
        // $arrData = array();
        // // $arrData['contrectFarmList'];
        // while ($rs = mysql_fetch_assoc($sqlFarmList)){
            //     $arrData['contrectFarmList'][] = $rs;
            // }
            // $connection->send(json_encode($arrData));
        global $db;
        $data = json_decode($data);
        $companyno = $data->r_companyno;
        $farmType = $data->r_type;
        // 농가 검색
        $query =  "SELECT FC.`no` AS contract_no, FC.ltms_status, FC.farm_no, FC.gps_type, F.* FROM farm F, farm_contract FC WHERE F.`no` = FC.farm_no AND F.lat <> 0 AND F.lon <> 0 AND FC.gps_type LIKE '$farmType' AND FC.contractor_company_no = $companyno AND contract_status = 1";
        $contractFarmList = $db->query($query);
        // 직영 계약 농가 추가
        $tmpQueryA = "SELECT company_no FROM ltms_gongyou WHERE to_companyy_no = $companyno";
        $gongyou = $db->query($tmpQueryA);
        foreach($gongyou as $rs){
            $jCompanyno = $rs['company_no'];
            $tmpQueryB = "SELECT `no` FROM farm WHERE jik_company_no = $jCompanyno ";
            $jiks = $db->query($tmpQueryB);
            foreach ($jiks as $jrs) {
                $jNo = $jrs['no'];
                $tmpQueryC = "SELECT FC.`no` AS contract_no, FC.ltms_status, FC.farm_no, FC.gps_type, F.*  FROM farm_contract FC, farm F 
                    WHERE  FC.farm_no = F.`no` AND FC.farm_no = $jNo AND FC.contractor_company_no = $jCompanyno AND contract_status = 1";
                 $contractFarmList = array_merge($db->query($tmpQueryC), $contractFarmList);
            }

        }
        // 사육중인 농가 계산
        $statusQuery = "SELECT COUNT(no) AS cnt FROM farm_contract FC WHERE FC.contractor_company_no = $companyno AND FC.ltms_status = 5 AND contract_status = 1";
        $feedCnt = $db->row($statusQuery);
        $arrData = array('contractFarmList' => $contractFarmList, 'feedCnt' => $feedCnt['cnt']);
        $connection->send(json_encode($arrData));
    }
}

?>