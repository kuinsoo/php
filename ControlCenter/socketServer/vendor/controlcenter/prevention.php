<?php
namespace ControlCenter;

class Prevention
{
   function __construct(){}
   
   public function contractPreventionList($connection, $data)
   {
      global $db;
      $jData = json_decode($data);
      $contractPreventionList = $db->query("SELECT *, ep.`no` AS ep_no, al.`no` AS ai_no,
                                             CASE 
                                                WHEN size_1 = 0 THEN '0'
                                                WHEN size_2 = 0 THEN '1'
                                                WHEN size_3 = 0 THEN '2'
                                                WHEN size_4 = 0 THEN '3'
                                                WHEN size_5 = 0 THEN '4'
                                                ELSE '0'
                                                END AS cnt_line
                                             FROM event_prevention ep , ai_list al WHERE ep.ai_type_no = al.`no` 
                                                   AND ep.company_code = $jData->company_no AND ep.prevention_state='on' ");
      $arrData = array('contractPreventionList' => $contractPreventionList);
      $connection->send(json_encode($arrData));
   }
}
