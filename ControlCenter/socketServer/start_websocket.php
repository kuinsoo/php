<?php

require_once __DIR__ . '/vendor/autoload.php';
// require_once __DIR__ . '/vendor/workerman/workerman/Autoloader.php';
use Workerman\Worker;

/* DB 연결을 위한 세팅 */
// $db=mysql_connect("localhost","root","web!@#") or die(mysql_error());
// mysql_select_db("egg") or die(mysql_error());

// Create a Websocket server
$ws_worker = new Worker("websocket://0.0.0.0:9070");

// 4 processes
$ws_worker->count = 4;
$Farm = new \ControlCenter\Farm;
$GPS = new \ControlCenter\GPS;
$Prevention = new \ControlCenter\Prevention;

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
    echo "New connection".$connection->id."\n";
    // 소켓 연결시 DB연결 
    global $db;
    $db = new \Workerman\MySQL\Connection('ltms.eemo.co.kr', '3306', 'root', 'web!@#', 'egg');
 };

// Emitted when data received
$ws_worker->onMessage = function($connection, $data) USE($Farm, $GPS, $Prevention)
{
    $item = json_decode($data);
    switch ($item->r_title) {
        case 'hi':
            // 핸드쉐이크
            $arrData = array('r_title'=>'hi','r_msg'=>'hello');
            $connection->send(json_encode($arrData));
            break;
        case 'contractFarmList':
            // 계약 농가 리스트 
            $Farm->contractFarmList($connection, $data);
            break;
        case 'contractCarList':
            // 계약 GPS 차량 리스트 
            $GPS->contractCarList($connection, $data);
            break;   
        case 'contractPreventionList':
            // 계약 방역 리스트 
            $Prevention->contractPreventionList($connection, $data);
            break;
        
        default:
            $arrData = array('r_title'=>'failed');
            $connection->send(json_encode($arrData));
            break;
    }
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
    echo "Connection closed\n";
};


// Run worker
Worker::runAll();

?>