const Event = class 
{
   constructor()
   {

   }

   /* 메뉴 마우스 올렸을 경우  */
   menuHover(element)
   {  // 이미지 이름 파싱
      const srcImage = element.getAttribute('src');
      let imageName = srcImage.substring(0,srcImage.indexOf(".png", 0));
      imageName = imageName.substr(imageName.indexOf("er/", 0)+3, imageName.length);
      switch (imageName)
      {  // Menu
         case 'farm'          : element.setAttribute('src', './img/controlcenter/farm_r.png'); break;
         case 'truck'         : element.setAttribute('src', './img/controlcenter/truck_r.png'); break;
         case 'prevention'    : element.setAttribute('src', './img/controlcenter/prevention_r.png'); break;
         case 'rotation'      : element.setAttribute('src', './img/controlcenter/rotation_r.png'); break;
         case 'setting'       : element.setAttribute('src', './img/controlcenter/setting_r.png'); break;
         // admin
         case 'admin'         : element.setAttribute('src', './img/controlcenter/admin_r.png');break;
      }
   }

   /* 메뉴 마우스 나갔을때 */
   menuUnhover(element)
   {  // 이미지 이름 파싱
      const srcImage = element.getAttribute('src');
      let imageName = srcImage.substring(0,srcImage.indexOf(".png", 0));
      imageName = imageName.substr(imageName.indexOf("er/", 0)+3, imageName.length);
      switch (imageName)
      {  // Menu
         case 'farm_r'        : element.setAttribute('src', './img/controlcenter/farm.png'); break;
         case 'truck_r'       : element.setAttribute('src', './img/controlcenter/truck.png'); break;
         case 'prevention_r'  : element.setAttribute('src', './img/controlcenter/prevention.png'); break;
         case 'rotation_r'    : element.setAttribute('src', './img/controlcenter/rotation.png'); break;
         case 'setting_r'     : element.setAttribute('src', './img/controlcenter/setting.png'); break;
         // admin
         case 'admin_r'       : element.setAttribute('src', './img/controlcenter/admin.png'); break;
      }
   }

   /* ==================================== 지도관련 이벤트 ============================================= */
   /* 지도타입 컨트롤의 지도 또는 스카이뷰 버튼을 클릭하면 호출되어 지도타입을 바꾸는 함수입니다 */
   setMapType(maptype) { 
      const roadmapControl = document.getElementById('btnRoadmap');
      const skyviewControl = document.getElementById('btnSkyview'); 
      if (maptype === 'roadmap') {
         roadmapControl.classList.toggle('btn-info', false);
         skyviewControl.classList.toggle('btn-info', true);
         map.setMapTypeId(kakao.maps.MapTypeId.ROADMAP); 
      } else {
         roadmapControl.classList.toggle('btn-info', true);
         skyviewControl.classList.toggle('btn-info', false);
         map.setMapTypeId(kakao.maps.MapTypeId.HYBRID);    
      }
   }

   /* 지도 확대, 축소 컨트롤에서 확대 버튼을 누르면 호출되어 지도를 확대하는 함수입니다 */
   zoomIn() {
      map.setLevel(map.getLevel() - 1);
   }

   /* 지도 확대, 축소 컨트롤에서 축소 버튼을 누르면 호출되어 지도를 확대하는 함수입니다 */
   zoomOut() {
      map.setLevel(map.getLevel() + 1);
   }

}