
const Common = class
{

  /* 모든 전역 변수 초기화 */
  removeAllMenu()
  {
    CenterFarm.removeFarmMenu();
    CenterGPS.removeGPSMenu();
    CenterPrevention.removePreventionMenu();
  }

  /* 맵 중앙이동 */
  setCenterMap = (lat, lng) => {
    let moveLatLon = new kakao.maps.LatLng(lat, lng);
    map.setCenter(moveLatLon);
  };

  /* 빈객체 */
   isEmpty = (value) => {
      if( value == "" || value == null || value == undefined || ( value != null && typeof value == "object" && !Object.keys(value).length ) ){
      return true
      }else{
      return false
      }
   };

   getCookieValue = (key) => {
      let cookieKey = key + "="; 
      let result = "";
      const cookieArr = document.cookie.split(";");
    
      for(let i = 0; i < cookieArr.length; i++) {
        if(cookieArr[i][0] === " ") {
          cookieArr[i] = cookieArr[i].substring(1);
        }
    
        if(cookieArr[i].indexOf(cookieKey) === 0) {
          result = cookieArr[i].slice(cookieKey.length, cookieArr[i].length);
          return result;
        }
      }
      return result;
    }
}