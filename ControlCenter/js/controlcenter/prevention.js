const Prevention = class
{
   /* 글로벌 배열들 (오브젝트들은 배열에 담아 추후 제거에 사용) */
   constructor()
   {
   }

   /* 전역 변수 */
   markers = new Array(); // 마커 배열
   labels = new Array(); // 라벨 배열
   /* ----------- 이벤트 처리 ----------- */
   
   /* 농가 메뉴 클릭 */
   preventionMenu()
   {
      if(CenterCommon.isEmpty(this.markers))
      {
         CenterCommon.removeAllMenu();
         if(confirm("방역 표시")){
            let jData = {'r_title':'contractPreventionList' , 'r_msg':'contractPreventionList', 'company_no':r_companyno};
            ws.send(JSON.stringify(jData));
         }else {return;}
      }else {
         if(confirm("방역표시 제거"))
         {
            CenterCommon.removeAllMenu();
         }else{return;}
      }
   }


   /* 농장 마커 생성 */
   contractPreventionList(_jData)
   {
      let arrJdata = _jData.contractPreventionList;

      
      // 마커제거
      this.removeMarker();

      // 마커생성
      this.addMarker(arrJdata);

   }

   /* 마커 생성 */
   addMarker(arrJdata)
   {
      let markerPositions = new Array();
      
      for (let idx = 0; idx < arrJdata.length; idx++) {
         const element = arrJdata[idx];
         let markerPosition = {
            latlng: new kakao.maps.LatLng(element['lat'], element['lng']),
            content:'<div class ="label" style="background: rgba(0,0,0,0.5);"><span class="left"></span><small><span class="center text-white px-1 pb-1">'+element['ai_name']+'</span></small><span class="right"></span></div>'
         };
         markerPositions.push(markerPosition);
      }

      // 마커를 생성
      for (var i = 0; i < markerPositions.length; i++) {
         var marker = new kakao.maps.Marker({
            map:map,
            position:markerPositions[i].latlng
         });
         var label = new kakao.maps.CustomOverlay({
            map:map,
            position : markerPositions[i].latlng,
            content : markerPositions[i].content
         });
         // 마커와 라벨 전역 배열에 담아두기
         this.markers.push(marker);
         this.labels.push(label);
      }
   }

   /* 마커 제거 */
   removeMarker()
   {
      if(this.markers.length >1 )
      {
         for (let i = 0; i < this.markers.length; i++) {
            const marker = this.markers[i];
            const label = this.labels[i];
            marker.setMap(null);
            label.setMap(null);
         }
      }
   }

   /* 방역메뉴 제거 */
   removePreventionMenu()
   {
      this.removeMarker(); // 마커 제거
      this.markers = new Array();
      this.labels = new Array();
      document.querySelector('#prevention-mode-side').classList.toggle('d-none',true); // 제거
   }

} 