const Farm = class
{
   /* 글로벌 배열들 (오브젝트들은 배열에 담아 추후 제거에 사용) */
   constructor()
   {
   }

   /* 전역 변수 */
   markers = new Array(); // 마커 배열
   labels = new Array(); // 라벨 배열

   /* ----------- 이벤트 처리 ----------- */  
   /* 농가 메뉴 클릭 */
   farmMenu()
   {
      if(CenterCommon.isEmpty(this.markers))
      {
         if(confirm("농장 마커 표시")){
            CenterCommon.removeAllMenu();
            document.querySelector('#farm-mode-side').classList.toggle('d-none', false); // 제거
            let jData = {'r_title':'contractFarmList' , 'r_msg':'contractFarmList', 'r_type':'%', 'r_companyno':r_companyno};
            ws.send(JSON.stringify(jData));
         }else {return;}
      }else {
         if(confirm("농장 마커 제거"))
         {
            CenterCommon.removeAllMenu();
         }else{return;}
      }
   }


   /* 농장 마커 생성 */
   contractFarmList(_jData)
   {
      let arrJdata = _jData.contractFarmList;

      this.deleteTable();//테이블 제거
      // 테이블 리스트 추가
      this.addTable(_jData)
      
      // 마커제거
      this.removeMarker();

      // 마커생성
      this.addMarker(arrJdata);

      for (let i = 0; i < this.markers.length; i++) {
         const element = this.markers[i];
         kakao.maps.event.addListener(element, 'click', function() {
            CenterFarm.showCustomOverlay(arrJdata[i]);
         });
         
      }
   }

   /* 테이블 생성 */
   addTable(_jData)
   {
      const arrJdata = _jData.contractFarmList; // 배열 값
      const arrCnt = arrJdata.length; // 배열 카운터
      const feedCnt = _jData.feedCnt; // 사육중인 농가 수

      const farmTable = document.getElementById('farm-list-data');
      
      document.querySelector('#farm-feed-cnt').innerHTML=feedCnt;
      document.querySelector('#farm-list-cnt').innerHTML=arrCnt;
      
      for (let idx = 0; idx < arrCnt; idx++) {
         const element = arrJdata[idx];

         let content = document.createElement('tr');
         
         content.style.height =  "42px";
         content.style.cursor = "pointer";
         content.setAttribute('class','tr-datas border-bottom border-light');
         

         // 맵중앙 이벤트 
         content.addEventListener('click', function(e) {
            let lat = element['lat'];
            let lng = element['lon'];
            let moveLatLon = new kakao.maps.LatLng(lat, lng);
            map.setCenter(moveLatLon); // 지도 중심 이동

            // 백그라운드 제거
            let trData = document.getElementsByClassName('tr-datas');
            for (let i = 0; i < trData.length; i++) {
               const element = trData[i];
               element.style.background = "";
            }
            // 클릭 백그라운드 변경            
            e.target.parentNode.style.background = "#c9c7c7";
            
         });
         content.innerHTML = `
            <th style="vertical-align: inherit;" class="p-1 text-center"  scope="row">${element['farm_name']}</th>
            <td style="vertical-align: inherit;" class="p-1" >${element['addr']}</td>`;
         
         // 사육 상태 체크 
         switch (element['ltms_status']) {
            case '5':
               content.innerHTML +=`<td style="vertical-align: inherit;" class="p-1 text-center"><i class="fas fa-check fa-xs text-default"></i></td>`;
               break;
               
            default:
               content.innerHTML +=`<td style="vertical-align: inherit;" class="p-1 text-center"></td>`;
               break;
         }

         // 카메라 상태 체크 
         content.innerHTML +=`<td style="vertical-align: inherit;" class="p-1 text-center"></td>`;

         farmTable.appendChild(content);
      }
   }

   /* 테이블 제거 */
   deleteTable()
   {
      let farmTable = document.getElementById('farm-list-data');
      farmTable.innerHTML  ="";
   }

   /* 마커 생성 */
   addMarker(arrJdata)
   {
      let markerPositions = new Array();
      
      for (let idx = 0; idx < arrJdata.length; idx++) {
         const element = arrJdata[idx];
         let imageSrc="";
         switch (element['gps_type']) {
            case 'CC':
               imageSrc = "./img/controlcenter/m_cc.png";
               break;
            case 'GPSSANRAN':
            case 'GPSYUK':
               imageSrc = "./img/controlcenter/m_gps.png";
               break;
            case 'PSSANRAN':
            case 'PSYUK':
               imageSrc = "./img/controlcenter/m_ps.png";
               break;
            default:
               imageSrc = "./img/controlcenter/m_cc.png";
               break;
         }

         const imageSize = new kakao.maps.Size(30, 70),
         imageOptions = {  
             spriteOrigin: new kakao.maps.Point(10, 0),    
             spriteSize: new kakao.maps.Size(50, 80)  
         };     
     

         let markerPosition = {
            latlng: new kakao.maps.LatLng(element['lat'], element['lon']),
            content:'<div class ="label" style="background: rgba(0,0,0,0.5);"><span class="left"></span><small><span class="center text-white px-1 pb-1">'+element['farm_name']+'</span></small><span class="right"></span></div>',
            image: new kakao.maps.MarkerImage(imageSrc, imageSize, imageOptions),
         };
         markerPositions.push(markerPosition);
      }

      // 마커를 생성
      for (let i = 0; i < markerPositions.length; i++) {
         let marker = new kakao.maps.Marker({
            map:map,
            position:markerPositions[i].latlng,
            image:markerPositions[i].image,
            clickable: true
         });
         let label = new kakao.maps.CustomOverlay({
            map:map,
            position : markerPositions[i].latlng,
            content : markerPositions[i].content
         });
         // 마커와 라벨 전역 배열에 담아두기
         this.markers.push(marker);
         this.labels.push(label);
         // marker.setClickable(true);
      }
   }

   /* 마커 제거 */
   removeMarker()
   {
      if(this.markers.length >1 )
      {
         for (let i = 0; i < this.markers.length; i++) {
            const marker = this.markers[i];
            const label = this.labels[i];
            marker.setMap(null);
            label.setMap(null);
         }
      }
   }

   /* 농가타입 선택 */
   selectFarmType(element)
   {
      let r_type = element.options[element.selectedIndex].value +"%";
      let jData = {'r_title':'contractFarmList' , 'r_msg':'타입별 농가검색', 'r_type':r_type, 'r_companyno': r_companyno};
      ws.send(JSON.stringify(jData));
   }

   /* 농가메뉴 제거 */
   removeFarmMenu()
   {
      this.deleteTable();//테이블 제거
      this.removeMarker(); // 마커 제거
      document.querySelector('#farm-list-cnt').innerHTML = 0;
      document.querySelector('#farm-feed-cnt').innerHTML = 0;
      document.querySelector('#farm-mode-side').classList.toggle('d-none',true); // 제거
      this.markers = new Array(); // 농장 마커 배열 초기화
      this.labels = new Array(); // 라벨 배열 초기화
   }

   /* 농가 오버레이 표기 */
   showCustomOverlay(farmData) 
   {
      const overlayboxs = document.querySelectorAll('.overlaybox');
      for (let i = 0; i < overlayboxs.length; i++) {
         const element = overlayboxs[i];
         element.parentNode.removeChild(element);
      }
      let content ="";
      const lat = farmData['lat'];
      const lng = farmData['lon'];
      content = '<div class="overlaybox" style="width:500px; height:700px; color:white; background:rgba(0,0,0,0.8)"' +
      '</div>';
      // 커스텀 오버레이가 표시될 위치입니다 
      let position = new kakao.maps.LatLng(lat, lng);  

      // 커스텀 오버레이를 생성합니다
      let customOverlay = new kakao.maps.CustomOverlay({
         position: position,
         content: content,
         xAnchor: -0.05,
         yAnchor: 0.5
      });

      // 커스텀 오버레이를 지도에 표시합니다
      customOverlay.setMap(map);
   }


} 