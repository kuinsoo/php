const GPS = class
{
   /* 글로벌 배열들 (오브젝트들은 배열에 담아 추후 제거에 사용) */
   constructor()
   {
}
   /* 전역 변수 */
   markers = new Array(); // 마커 배열
   labels = new Array(); // 라벨 배열
   intervalGPS;

   /* ----------- 이벤트 처리 ----------- */
   gpsMenu()
   {
      if(CenterCommon.isEmpty(this.intervalGPS)){
         CenterCommon.removeAllMenu();
         document.querySelector('#GPS-mode-side').classList.toggle('d-none', false); // 제거
         if(confirm("10초 후 GPS 추적 시작")){
            this.intervalGPS = setInterval(() => {
               let jData = {'r_title':'contractCarList' , 'r_msg':'contractCarList'};
               ws.send(JSON.stringify(jData));
            }, 10000);
         }else {
            return;
         }
         
      }else {
         if(confirm("GPS 추적 종료")){
            CenterCommon.removeAllMenu();
         }
      }
   }

   /* GPS 마커 생성 */
   contractCarList(_jData)
   {
      let arrJdata = _jData.contractCarList; // 서버 : GPS 최신 리스트
      
      // 마커삭제
      this.removeMarker();

      // 마커생성
      this.addMarker(arrJdata);
     
   }

   /* 마커생성 */
   addMarker(arrJdata)
   {
      let markerPositions = new Array();

      for (let idx = 0; idx < arrJdata.length; idx++) {
         const element = arrJdata[idx];

         let imageSrc = "./img/controlcenter/m_truck1.png",
         imageSize = new kakao.maps.Size(30, 80),
         imageOptions = {  
             spriteOrigin: new kakao.maps.Point(10, 0),    
             spriteSize: new kakao.maps.Size(50, 100)  
         };     
     
         // 마커이미지와 마커를 생성합니다
         let markerPosition = {
            latlng: new kakao.maps.LatLng(element['lat'], element['lon']),
            content:'<div class ="label" style="background: rgba(0,0,0,0.5);"><span class="left"></span><small><span class="center text-white px-1 pb-1">'+element['car_num']+'</span></small><span class="right"></span></div>',
            image: new kakao.maps.MarkerImage(imageSrc, imageSize, imageOptions)
         };
         markerPositions.push(markerPosition);
      }
      // 마커를 생성
      for (let i = 0; i < markerPositions.length; i++) {
         let marker = new kakao.maps.Marker({
            map:map,
            position:markerPositions[i].latlng,
            image:markerPositions[i].image
         });
         let label = new kakao.maps.CustomOverlay({
            map:map,
            position : markerPositions[i].latlng,
            content : markerPositions[i].content
         });
         // 마커와 라벨 전역 배열에 담아두기
         this.markers.push(marker);
         this.labels.push(label);
      }
   }


   /* 마커제거 */
   removeMarker()
   {
      if(this.markers.length >1 )
      {
         for (let i = 0; i < this.markers.length; i++) {
            const marker = this.markers[i];
            const label = this.labels[i];
            marker.setMap(null);
            label.setMap(null);
         }
      }
   }

   /* GPS 메뉴 제거 */
   removeGPSMenu()
   {
      clearInterval(this.intervalGPS);
      this.intervalGPS={};
         // 마커삭제
      this.removeMarker();
      this.markers = new Array();
      this.labels = new Array(); 
      document.querySelector('#GPS-mode-side').classList.toggle('d-none', true); // 제거
   }


}