<?php
   /* DB 연결을 위한 세팅 */
   $rootDir = $_SERVER['DOCUMENT_ROOT'];
   include_once($rootDir . '/super/inc_header.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>관제센터</title>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
   <!-- Bootstrap core CSS -->
   <link href="./css/bootstrap.min.css" rel="stylesheet">
   <!-- Material Design Bootstrap -->
   <link href="./css/mdb.min.css" rel="stylesheet">
   <!-- Your custom styles (optional) -->
   <link href="./css/style.css" rel="stylesheet">
</head>
<body>
   <!-- 메뉴바 -->
   <div id="daumGnb">
      <!-- 농장 -->
      <ul id="gnbMenu">
         <li id="" class="bg-dark"><a href=""></a></li>
         <li id="" class="bg-dark"><a href=""></a></li>
         <li id="" class=""><a href="javascript:CenterFarm.farmMenu();"><img src="./img/controlcenter/farm.png"  alt="Non"  onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
         <li id="" class=""><a href="javascript:CenterGPS.gpsMenu();"><img src="./img/controlcenter/truck.png"  alt="Non" onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
         <li id="" class=""><a href="javascript:CenterPrevention.preventionMenu();"><img src="./img/controlcenter/prevention.png"  alt="Non" onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
         <li id="" class=""><a href=""><img src="./img/controlcenter/rotation.png"  alt="Non" onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
         <li id="" class=""><a href=""><img src="./img/controlcenter/setting.png"  alt="Non" onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
      </ul>
      <!-- 어드민 -->
      <ul id="adminMenu">
         <li id="" class=""><a href=""><img src="./img/controlcenter/admin.png"  alt="Non" onmouseover="CenterEvent.menuHover(this);" onmouseout="CenterEvent.menuUnhover(this);"></a></li>
      </ul>
   </div>
   

   <div id="sideFarmMenu" class="sideMenu">
      <section class="section-preview">
         <div class="form-check form-check-inline">
         <input type="radio" class="form-check-input" id="materialInline1" name="inlineMaterialRadiosExample">
         <label class="form-check-label" for="materialInline1">전체</label>
         </div>

         <div class="form-check form-check-inline">
         <input type="radio" class="form-check-input" id="materialInline2" name="inlineMaterialRadiosExample">
         <label class="form-check-label" for="materialInline2">GPS</label>
         </div>

         <div class="form-check form-check-inline">
         <input type="radio" class="form-check-input" id="materialInline3" name="inlineMaterialRadiosExample">
         <label class="form-check-label" for="materialInline3">PS</label>
         </div>  

         <div class="form-check form-check-inline">
         <input type="radio" class="form-check-input" id="materialInline4" name="inlineMaterialRadiosExample">
         <label class="form-check-label" for="materialInline4">CC</label>
         </div>
      </section>
   </div>
   <!-- top menu -->
   <!-- <div id="topMenu" class="input-group md-form form-sm form-1 pl-0 my-0">
      <input class="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search">
      <div class="input-group-prepend">
         <span class="input-group-text cyan lighten-2" id="basic-text1"><i class="fas fa-search text-white"
            aria-hidden="true"></i></span>
      </div>
   </div> -->

   <div id="map" style="width:100%;height:100vh;"></div>

   <!-- 지도타입 컨트롤 -->
   <div class="custom_typecontrol radius_border">
      <span id="btnRoadmap" class="selected_btn " onclick="CenterEvent.setMapType('roadmap')">지도</span>
      <span id="btnSkyview" class="selected_btn bg-dark" onclick="CenterEvent.setMapType('skyview')">스카이뷰</span>
   </div>


   <!-- JQuery -->
   <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
   <!-- Bootstrap tooltips -->
   <script type="text/javascript" src="./js/popper.min.js"></script>
   <!-- Bootstrap core JavaScript -->
   <script type="text/javascript" src="./js/bootstrap.min.js"></script>
   <!-- MDB core JavaScript -->
   <script type="text/javascript" src="./js/mdb.min.js"></script>
   <!-- Custom JavaScript -->
   <script type="text/javascript" src="./js/controlcenter/farm.js"></script>
   <script type="text/javascript" src="./js/controlcenter/gps.js"></script>
   <script type="text/javascript" src="./js/controlcenter/prevention.js"></script>
   <script type="text/javascript" src="./js/controlcenter/event.js"></script>
   <script type="text/javascript" src="./js/controlcenter/common.js"></script>

   <!-- KaKao Map -->
   <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=3ace3ba89932ca29a94ad3eead2ac0ca"></script>

   <script>
      /* Farm Class 객체 생성 */
      const CenterFarm = new Farm();
      const CenterGPS = new GPS();
      const CenterPrevention = new Prevention();
      const CenterEvent = new Event();
      const CenterCommon = new Common();
      
      /* 지도를 담을 영역의 DOM 레퍼런스 */
      const container = document.getElementById('map');
      const options = {//지도를 생성할 때 필요한 기본 옵션
         center: new kakao.maps.LatLng(36.35859831663171, 128.09974913611455),//지도의 중심좌표.
         level: 7 //지도의 레벨(확대, 축소 정도)
      };

      /* 맵에 객체 생성 */
      const map = new kakao.maps.Map(container, options);

      

      /* =========================================================         맵 세팅        ========================================================= */

      /* HTML 로드 후 로딩 */
      window.onload = function()
      {
         mapInit(map);
      }

      /* 맵 세팅 */
      function mapInit(map) {
         // 스카이뷰 설정
         map.setMapTypeId(kakao.maps.MapTypeId.HYBRID);

         // 마커가 표시될 위치입니다. 
         markerPositions = 
         [ 
            {
            latlng: new kakao.maps.LatLng(36.35859831663171, 128.09974913611455),
            content:'<div class ="label" style="background: rgba(0,0,0,0.5);"><span class="left"></span><small><span class="center text-white px-1 pb-1">한울</span></small><span class="right"></span></div>'
            },
         ]
         let markerPosition = new kakao.maps.LatLng(36.35859831663171, 128.09974913611455);

         // 마커를 생성
         for (let i = 0; i < markerPositions.length; i++) {
            let marker = new kakao.maps.Marker({
               map:map,
               position:markerPositions[i].latlng
            });
            let customOverlay = new kakao.maps.CustomOverlay({
               map:map,
               position : markerPositions[i].latlng,
               content : markerPositions[i].content
            });
         }
      }
      /* =========================================================         이벤트         ========================================================= */

      /* =========================================================      웹 소켓 영역      ========================================================= */
      const ws = new WebSocket('ws://ltms.eemo.co.kr:9070');

      /* 최초 연결 */
      ws.onopen = function()
      {
         let hiData = {'r_title':'hi' , 'r_msg':'hi'};
         ws.send(JSON.stringify(hiData));
      };

      /* 메시지 응답 */
      ws.onmessage = function(event)
      {
         const _jData = JSON.parse(event.data);
         let objName = Object.getOwnPropertyNames(_jData);
         switch (objName[0]) {
            /* ----------- Farm ----------- */
            case 'contractFarmList':
               CenterFarm.contractFarmList(_jData);
               break;

            /* ----------- GPS ----------- */
            case 'contractCarList':
               CenterGPS.contractCarList(_jData);
               break;
            
            /* ----------- Prevention ----------- */
            case 'contractPreventionList':
               CenterPrevention.contractPreventionList(_jData);
               break;
            default:
               console.log(_jData);
               break;
         }
      }; 
  </script>
</body>
</html>