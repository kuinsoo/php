<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>관제센터</title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="./css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="./css/style.css" rel="stylesheet">
</head>
<body>
   <style>
      * { font-size: 11px !important; font-family: 'D2Coding';} /* 폰트 */
      .invisible-scrollbar::-webkit-scrollbar {display: none;} /* 스크롤제거 */
      #farm-list-data tr:hover{background: #c9c7c7;} /* data table 마우스 오버 */
   </style>
   <!--Navbar-->
   <nav id="top-nav" class="navbar navbar-expand-lg navbar-dark primary-color" style="background: rgba(0, 0, 0, 0.8) !important; height: 3vh;">

      <!-- Navbar brand -->
      <a class="navbar-brand" href="/super/controlcenter/controlcenter" style="font-size: 12px;margin-left: 10px;"><i class="fas fa-home fa-sm  "></i> 양계인 관제시스템</a>
   
      <!-- Links -->
      <ul class="navbar-nav mr-auto" style="margin-left: 15%;">
         <li class="nav-item" style="width:100px">
            <a class="nav-link" href="javascript:CenterFarm.farmMenu();">농장</a>
         </li>
         <li class="nav-item" style="width:100px">
            <a class="nav-link" href="javascript:CenterGPS.gpsMenu();">GPS</a>
         </li>
         <li class="nav-item" style="width:100px">
            <a class="nav-link" href="javascript:CenterPrevention.preventionMenu();">방역</a>
         </li>
         <!-- Dropdown -->
         <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
            </div>
         </li> -->
   
      </ul>
      <!-- Links -->
   

      <form class="form-inline">
         <div class="md-form my-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
         </div>
      </form>
      </div>
      <!-- Collapsible content -->
   
   </nav>
   <!--/.Navbar-->

   <div id="content-wrap" style="content: ''; display: block; clear: both; height: 97vh;">
      <aside style="float: left; width: 20%; height: 100%; padding: 5px 0px 5px 5px;">
         <!-- 농가모드 -->
         <div id="farm-mode-side" class="" style="background: rgba(0, 0, 0, 0.0); height: 100%;">
            <nav>
               <select id="select-farm-type" class="custom-select custom-select-sm w-25 m-2 text-white bg-default" style="font-size: 11px;" onchange="CenterFarm.selectFarmType(this)">
                  <option value="%" selected>전체</option>
                  <option value="GPS">GPS</option>
                  <option value="PS">PS</option>
                  <option value="CC">CC</option>
               </select>
               
               <div class="float-right text-right" style="width: 60%;">
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">사육현황</button>
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">CCTV</button>
               </div>
            </nav>
               
            <section class="border" style=" height: 89vh;">
               <table class="table table-borderless">
                  <colgroup>
                     <col width="20%"/>
                     <col width="60%"/>
                     <col width="10%"/>
                     <col width="10%"/>
                  </colgroup>
                  <thead>
                    <tr  class="border-bottom border-light">
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가명</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가주소</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>사육</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>CCTV</strong></th>
                    </tr>
                    <tr></tr>
                  </thead>
               </table>
               <div class="invisible-scrollbar" style="overflow-y:scroll; height: 80vh;">
                  <table class="table table-borderless">
                     <colgroup>
                        <col width="20%"/>
                        <col width="60%"/>
                        <col width="10%"/>
                        <col width="10%"/>
                     </colgroup>
                     <tbody id="farm-list-data">
                        <!-- 테이블 데이터 들어가는 곳 -->
                     </tbody>
                  </table>
               </div>
            </section>
            <div class="text-right">
               <small class="mr-2">사육 농가수 : <span id="farm-feed-cnt" class="text-default">0</span></small>
               <small class="mr-2">검색 총 농가수 : <span id="farm-list-cnt" class="text-primary">0</span></small>
            </div>
         </div>

         <!-- GPS 모드 -->
         <div id="GPS-mode-side" class="d-none" style="background: rgba(0, 0, 0, 0.0); height: 100%;">
            <nav>
               <select id="select-GPS-type" class="custom-select custom-select-sm w-25 m-2 text-white bg-default" style="font-size: 11px;" onchange="CenterFarm.selectFarmType(this)">
                  <option value="%" selected>GPS</option>
                  <option value="GPS">GPS</option>
                  <option value="PS">PS</option>
                  <option value="CC">CC</option>
               </select>
               
               <div class="float-right text-right" style="width: 60%;">
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">GPS</button>
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">GPS</button>
               </div>
            </nav>
               
            <section class="border" style=" height: 89vh;">
               <table class="table table-borderless">
                  <colgroup>
                     <col width="20%"/>
                     <col width="60%"/>
                     <col width="10%"/>
                     <col width="10%"/>
                  </colgroup>
                  <thead>
                    <tr  class="border-bottom border-light">
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가명</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가주소</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>사육</strong></th>
                      <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>CCTV</strong></th>
                    </tr>
                    <tr></tr>
                  </thead>
               </table>
               <div class="invisible-scrollbar" style="overflow-y:scroll; height: 80vh;">
                  <table class="table table-borderless">
                     <colgroup>
                        <col width="20%"/>
                        <col width="60%"/>
                        <col width="10%"/>
                        <col width="10%"/>
                     </colgroup>
                     <tbody id="GPS-list-data">
                        <!-- 테이블 데이터 들어가는 곳 -->
                     </tbody>
                  </table>
               </div>
            </section>
            <div class="text-right">
               <small class="mr-2">사육 농가수 : <span id="GPS-feed-cnt" class="text-default">0</span></small>
               <small class="mr-2">검색 총 농가수 : <span id="GPS-list-cnt" class="text-primary">0</span></small>
            </div>
         </div>

         <!-- Prevention 모드 -->
         <div id="prevention-mode-side" class="d-none" style="background: rgba(0, 0, 0, 0.0); height: 100%;">
            <nav>
               <select id="select-prevention-type" class="custom-select custom-select-sm w-25 m-2 text-white bg-default" style="font-size: 11px;" onchange="CenterFarm.selectFarmType(this)">
                  <option value="%" selected>GPS</option>
                  <option value="GPS">GPS</option>
                  <option value="PS">PS</option>
                  <option value="CC">CC</option>
               </select>
               
               <div class="float-right text-right" style="width: 60%;">
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">GPS</button>
                  <button type="button" class="btn btn-sm btn-rounded text-white " style="padding-top: 6px;padding-bottom: 6px; background: rgba(0, 0, 0, 0.8) !important;">GPS</button>
               </div>
            </nav>
               
            <section class="border" style=" height: 89vh;">
               <table class="table table-borderless">
                  <colgroup>
                     <col width="20%"/>
                     <col width="60%"/>
                     <col width="10%"/>
                     <col width="10%"/>
                  </colgroup>
                  <thead>
                     <tr  class="border-bottom border-light">
                        <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가명</strong></th>
                        <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>농가주소</strong></th>
                        <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>사육</strong></th>
                        <th scope="col" class="py-3 px-1 " style="font-size:11px; text-align: center;"><strong>CCTV</strong></th>
                     </tr>
                     <tr></tr>
                  </thead>
               </table>
               <div class="invisible-scrollbar" style="overflow-y:scroll; height: 80vh;">
                  <table class="table table-borderless">
                     <colgroup>
                        <col width="20%"/>
                        <col width="60%"/>
                        <col width="10%"/>
                        <col width="10%"/>
                     </colgroup>
                     <tbody id="prevention-list-data">
                        <!-- 테이블 데이터 들어가는 곳 -->
                     </tbody>
                  </table>
               </div>
            </section>
            <div class="text-right">
               <!-- <small class="mr-2">사육 농가수 : <span id="prevention-feed-cnt" class="text-default">0</span></small> -->
               <small class="mr-2">총 방역 : <span id="prevention-list-cnt" class="text-primary">0</span></small>
            </div>
         </div>
         
      </aside>
      
      <section class="position-relative" style="float: right; width: 80%; height: 100%; padding: 5px;">
         <div class="map_wrap"style="width: 100%;height: 100%;">
            <div id="map" style="width: 100%;height: 100%;"></div>
            <!-- 지도타입 컨트롤 -->
            <div class="custom_typecontrol radius_border position-absolute" style="top: 5px; right: 20px;z-index: 1;">
               <span id="btnRoadmap" class="btn btn-info" style="opacity: 0.8;" onclick="CenterEvent.setMapType('roadmap')">지도</span>
               <span id="btnSkyview" class="btn" style="opacity: 0.8;" onclick="CenterEvent.setMapType('skyview')">스카이뷰</span>
            </div>
         </div>
      </section>
   </div>




<!-- JQuery -->
<script type="text/javascript" src="./js/jquery.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="./js/popper.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="./js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="./js/mdb.js"></script>
<!-- Custom JavaScript -->
<script type="text/javascript" src="./js/controlcenter/farm.js"></script>
<script type="text/javascript" src="./js/controlcenter/gps.js"></script>
<script type="text/javascript" src="./js/controlcenter/prevention.js"></script>
<script type="text/javascript" src="./js/controlcenter/event.js"></script>
<script type="text/javascript" src="./js/controlcenter/common.js"></script>

<!-- KaKao Map -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=3ace3ba89932ca29a94ad3eead2ac0ca"></script>

<script>
/* 지도를 담을 영역의 DOM 레퍼런스 */
const container = document.getElementById('map');
const options = {//지도를 생성할 때 필요한 기본 옵션
      center: new kakao.maps.LatLng(36.35859831663171, 128.09974913611455),//지도의 중심좌표.
      level: 7 //지도의 레벨(확대, 축소 정도)`
};
/* 맵에 객체 생성 */
const map = new kakao.maps.Map(container, options);

/* Farm Class 객체 생성 */
const CenterFarm = new Farm();
const CenterGPS = new GPS();
const CenterPrevention = new Prevention();
const CenterEvent = new Event();
const CenterCommon = new Common();
const r_companyno = CenterCommon.getCookieValue('ltms_company_no'); // 쿠기 회사 넘버
/* =========================================================         맵 세팅        ========================================================= */

/* HTML 로드 후 로딩 */
window.onload = function()
{
   mapInit(map);
   let jData = {'r_title':'contractFarmList' , 'r_msg':'contractFarmList', 'r_type':'%', r_companyno};
   ws.send(JSON.stringify(jData));
}

/* 맵 세팅 */
function mapInit(map) {
   // 스카이뷰 설정
   map.setMapTypeId(kakao.maps.MapTypeId.HYBRID);

   // 마커가 표시될 위치입니다. 
   markerPositions = 
   [ 
      {
         latlng: new kakao.maps.LatLng(36.35859831663171, 128.09974913611455),
         content:'<div class ="label" style="background: rgba(0,0,0,0.5);"><span class="left"></span><small><span class="center text-white px-1 pb-1">한울</span></small><span class="right"></span></div>'
      },
   ]
   let markerPosition = new kakao.maps.LatLng(36.35859831663171, 128.09974913611455);

   // 마커를 생성
   for (let i = 0; i < markerPositions.length; i++) {
      let marker = new kakao.maps.Marker({   
         map:map,
         position:markerPositions[i].latlng
      });
      let customOverlay = new kakao.maps.CustomOverlay({
         map:map,
         position : markerPositions[i].latlng,
         content : markerPositions[i].content
      });
   }
}
   /* =========================================================         이벤트         ========================================================= */

   /* =========================================================      웹 소켓 영역      ========================================================= */
   const ws = new WebSocket('ws://localhost:9070');

   /* 최초 연결 */
   ws.onopen = function()
   {
      let hiData = {'r_title':'hi' , 'r_msg':'hi'};
      ws.send(JSON.stringify(hiData));
   };

   /* 메시지 응답 */
   ws.onmessage = function(event)
   {
   const _jData = JSON.parse(event.data);
      let objName = Object.getOwnPropertyNames(_jData);
   switch (objName[0]) {
      /* ----------- Farm ----------- */
      case 'contractFarmList':
         CenterFarm.contractFarmList(_jData);
         break;

      /* ----------- GPS ----------- */
      case 'contractCarList':
         CenterGPS.contractCarList(_jData);
         break;
      
      /* ----------- Prevention ----------- */
      case 'contractPreventionList':
         CenterPrevention.contractPreventionList(_jData);
         break;

      default:
         console.log(_jData);
         break;
      }
   }; 
</script>
</body>
</html>