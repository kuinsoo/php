<?php
require __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
$worker = new Worker();
$worker->onWorkerStart = function(){
    $http = new Workerman\Http\Client();

    // $http->get('http://kuinsoo.github.io', function($response){
    //         var_dump($response->getStatusCode());
    //         echo $response->getBody();
    //     }, function($exception){
    //         echo $exception;
    //     });

    // $http->post('http://kuinsoo.github.io', ['key1'=>'value1','key2'=>'value2'], function($response){
    //         var_dump($response->getStatusCode());
    //         echo $response->getBody();
    //     }, function($exception){
    //         echo $exception;
    //     });

    $http->request('http://ltms.eemo.co.kr/super/controlcenter/controlcenter.php', [
            'method'  => 'POST',
            'version' => '1.1',
            'headers' => ['Connection' => 'keep-alive'],
            'data'    => ['key1' => 'value1', 'key2'=>'value2'],
            'success' => function ($response) {
                // print_r($response);
                echo $response->getBody();
            },
            'error'   => function ($exception) {
                echo $exception;
            }
        ]);
};
Worker::runAll();