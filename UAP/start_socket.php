<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
use Workerman\Lib\Timer;


// Create a Websocket server
$ws_worker = new Worker("websocket://0.0.0.0:8601");

// 4 processes
$ws_worker->count = 4;
// 4 processes
$web->count = 4;

class Msg {
   public $type, $msg;

}

$_sendData = new Msg;

$ws_worker->onWorkerStart = function($ws_worker) {
   $time_interval = 1;
   Timer::add($time_interval,
      function() use ($ws_worker)
      {
         foreach($ws_worker->connections as $connection) {
            if($connection->channelTime) {
               $_sendData = new Msg;
               $_sendData->type = "start";
               $_sendData->msg = "[".date("Y-m-d H:i:s")."] 시작 : ".$connection->id;
               $connection->send(json_encode($_sendData));
            }
         }

      }
   );
};

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
   $connection->channelTime = false;
   $JSONConnected = 
   [
      "type"   => "connected",
      "msg"    => "접속성공",
   ];
   $JSONEncode = json_encode($JSONConnected);

   $connection->send($JSONEncode);
 };
 

// Emitted when data received
$ws_worker->onMessage = function($connection, $data)
{

   global $timer_id , $_sendData;
   $decodeData = json_decode($data);
    switch ($decodeData->inputText) {
       case '시작':
         $connection->channelTime = true;
        
         
         break;
         
      case '정지':
         // 정비가 한번에 안되는 문제 해결
         $connection->channelTime = false;
         break;
       
      case '멈춰':
         Timer::delAll();
         $_sendData->type = "allstop";
         $_sendData->msg = "모두멈춰";
         $connection->send(json_encode($_sendData));
         break;

       default:
          # code...
          break;
    }
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
    echo "Connection closed\n";
};

// Run worker
Worker::runAll();