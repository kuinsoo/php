<?php 
use Workerman\Worker;
require_once __DIR__ . '/vendor/workerman/Workerman/Autoloader.php';
require_once __DIR__ . '/vendor/autoload.php';

$worker = new Worker('websocket://0.0.0.0:8610');
// 4 processes
$ws_worker->count = 4;
// 4 processes
$web->count = 4;

$worker->onWorkerStart = function($worker)
{
    // 전역 변수에 감소 해독 저장(특정 유형의 정적 멤버를 어떤 종류의 정적 멤버에 저장할 수도 있음)
    $db = new \Workerman\MySQL\Connection('127.0.0.1', '8000', 'kuinsoo', '0701rnfma4', 'kkuisland');
};
$worker->onMessage = function($connection, $data)
{
    // 전역 변수를 통해 DB 인스턴스 할당
    global $db;
    // Execute SQL
    $all_tables = $db->query('show tables');
    $connection->send(json_encode($all_tables));
};
// Run worker
Worker::runAll();