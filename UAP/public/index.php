<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TimerServer</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  
</head>
<body>
  <div class="form-group">
    <label for="">Message</label>
    <input type="text" class="form-control w-25" name="inp-msg" id="inp-msg" aria-describedby="helpId" value="시작" placeholder="메시지 입력">
    <input type="button" value="전송" onclick="_sendMsg();">
    <small id="helpId" class="form-text text-muted">메시지를 입력하면 타이머 작동</small>
  </div>
  <textarea name="txa-log" id="txa-log" cols="30" rows="10" class="w-100" readonly></textarea>


  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>
</html>

<script>
  window.onload = function()
  {

  }

  const ws = new WebSocket('ws://http://localhost:8601');

  ws.onmessage = function (event)
  {
    console.log(event.data);
    
  }
  // ws.onmessage  = function (event)
  //   {
  //     var _jsonData = JSON.parse(event.data);
  //     switch (_jsonData.type) {
  //       case 'start':
  //         workMsg(_jsonData);
  //         break;
  //       case 'stop':
  //         workMsg(_jsonData);
  //         break;
      
  //       case 'allstop':
  //         workMsg(_jsonData);
  //         break;
  //       default:
  //         break;
  //     }
  //   }
  //   // ws.send('test');

  // ws.onopen = function(event) {
  //   var _inpMessage = document.getElementById('inp-msg').value;
  //   var sendJson = {
  //     "type": "send",
  //     "inputText" : _inpMessage
  //   };
  //   console.log(sendJson);
  //   ws.send(JSON.stringify(sendJson));
  // }

  // /* 전송버튼 클릭 */
  // function _sendMsg() {
  //   // 소켓 객체 생성
  //   // 접속 완료

  // }  
  // function workMsg(data) {
  //   var txaLog = document.getElementById('txa-log');
  //   var text = data.msg;
  //   var textnode = document.createTextNode(text+"\n");
  //   document.getElementById('txa-log').appendChild(textnode);
  // }
</script>