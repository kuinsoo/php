<?php
/**
 * site 정보
 * https://www.bitmex.com/app/wsAPI
 * https://medium.com/@icehongssii/%ED%8C%8C%EC%9D%B4%EC%8D%AC%EC%9C%BC%EB%A1%9C-bitmex-%EA%B1%B4%EB%93%9C%EB%A6%AC%EA%B8%B0-1-bitmex-websock%EC%97%B0%EA%B2%B0%ED%95%B4%EC%84%9C-ohlcv%EA%B0%80%EC%A0%B8%EC%98%A4%EA%B8%B0-97e932253280
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TimerServer</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="/css/style.css" rel="stylesheet">

</head>
<body>

<div class="md-form mb-4 pink-textarea active-pink-textarea">
  <i class="fas fa-angle-double-right prefix"></i>
  <textarea id="form21" class="md-textarea form-control" rows="30"></textarea>
  <label for="form21">Material textarea with a colorful prefix on :focus state</label>
</div>





  <!-- JQuery -->
  <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="/js/mdb.min.js"></script>
</body>
</html>

<script>

  var command = 'subscribe=trade:XBTUSD';
  var endpoint = 'wss://www.bitmex.com/realtime?'+command;

  socket = new WebSocket('wss: //www.bitmex.com/realtime');

  socket.onmessage = function(event) {
    // console.log(event.data);

    var _jsonData = JSON.parse(event.data);
    var  test = _jsonData.data;
    test.forEach(element => {
      console.log(element);
      document.getElementById('form21').prepend(element.price+"\n");
    });
    
  }

 
</script>

