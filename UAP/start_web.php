<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
use Workerman\WebServer;
use Workerman\Autoloader;
use PHPSocketIO\SocketIO;

// WebServer
$web = new WebServer("http://0.0.0.0:80");

// Set the root of domains
$web->addRoot('localhost', './public');
// $web->addRoot('www.another_domain.com', '/another/path/Web');
// run all workers
// Worker::runAll();
