<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;

// Create a Websocket server
$ws_worker = new Worker("websocket://0.0.0.0:8601");

// 4 processes
$ws_worker->count = 4;

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
    echo "New connection\n";
    global $db;
    $db = new \Workerman\MySQL\Connection('host', 'port', 'user', 'password', 'db_name');
 };

// Emitted when data received
$ws_worker->onMessage = function($connection, $data)
{
   setInterval(function($connection){
      $connection->send(date('Y-m-d'));
   }, 1000);
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
    echo "Connection closed\n";
};

function setInterval($f, $milliseconds)
{
   $seconds=(int)$milliseconds/1000;
   while(true)
   {
      $f();
      sleep($seconds);
   }
}
// Run worker
Worker::runAll();